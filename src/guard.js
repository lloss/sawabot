module.exports = (cmd, guardConfig) => {
  const allowedRoles = guardConfig[cmd.name].roles;

  return {
    ...cmd,
    guard(roles) {
      return roles && allowedRoles.some(name => roles.exists("name", name));
    }
  };
};
