const fs = require("fs");
const { downloadAsync, existsAsync } = require("../utils");
const Fuse = require("fuse.js");

const distPath = `${__dirname}/../../data/stickers`;

const stickers = fs
  .readdirSync(distPath)
  .filter(name => name !== ".gitkeep")
  .map(name => ({
    name: name.replace(".png", "")
  }));

const fuse = new Fuse(stickers, {
  keys: ["name"],
  distance: 2,
  shouldSort: true,
  minMatchCharLength: 3
});

let listTime = 0;
const ANTISPAM_TIME = 60000;

const parseCommand = msg => /^стикер[ ]*(.*)[ ]*?/gim.exec(msg);

const isImage = name => !!/\.png$/.exec(name);

const sendStickerList = message => {
  if (listTime + ANTISPAM_TIME > Date.now()) {
    return void message.reply("Не");
  }

  message.reply(
    ` **Список стикеров:**  ${stickers
      .map((s, i) => `**${i + 1}** ${s.name}`)
      .join(", ")}`
  );

  listTime = Date.now();
};

const saveStickerCommand = async (url, dest, name, message) => {
  try {
    await downloadAsync(url, dest);
    message.reply("Стикер добавлен");

    stickers.push({ name });
  } catch (e) {
    console.error(e);
    message.reply("Произошла ошибка");
  }
};

module.exports = {
  name: "sticker",
  test: message => parseCommand(message) !== null,
  exec: async message => {
    let [_, name] = parseCommand(message.content);

    const dest = `${distPath}/${name}.png`;
    const exists = await existsAsync(dest);
    const attachement = message.attachments.first();

    if (name && attachement && isImage(attachement.url)) {
      if (exists) {
        return void message.reply("Стикер с таким именем уже существует");
      }

      return saveStickerCommand(attachement.url, dest, name, message);
    } else {
      if (name === "лист") {
        return void sendStickerList(message);
      }

      let stickerPath = dest;
      let isExists = exists;

      if (!isExists) {
        const result = fuse.search(name);

        if (result.length) {
          stickerPath = `${distPath}/${result[0].name}.png`;
          isExists = true;
          name = result[0].name;
        }
      }

      if (!isExists) {
        return void message.reply("Стикер не найден");
      }

      message.channel.send(`<@${message.author.id}>, ${name}`, {
        files: [stickerPath]
      });
    }

    message.delete();
  }
};
