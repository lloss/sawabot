const Discord = require("discord.js");
const conf = require("./config");
const guard = require("./guard");
const commands = require("./commands").map(c => guard(c, conf.commands.guard));

const client = new Discord.Client();

client.on("ready", () => {
  client.user.setActivity("Как захватить мир", {
    type: "WATCHING"
  });

  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", msg => {
  commands.forEach(cmd => {
    if (!cmd.test(msg.content)) return;

    if (cmd.guard(msg.member.roles)) {
      cmd.exec(msg);
    } else {
      msg.reply("Недостаточно прав");
    }
  });
});

client.login(conf.token);
