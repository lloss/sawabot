const { promisify } = require("util");
const https = require("https");
const fs = require("fs");

module.exports = {
  downloadAsync: (url, dist) =>
    new Promise((resolve, reject) => {
      const file = fs.createWriteStream(dist);

      const request = https.get(url, r => {
        r.pipe(file);

        file.on("finish", cb => {
          file.close(cb);
          resolve();
        });
      });

      request.on("error", reject);
    }),

  existsAsync: promisify(fs.exists)
};
